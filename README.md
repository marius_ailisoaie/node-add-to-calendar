# node-add-to-calendar

Generate calendar events for: 
- iCal
- Google 
- Outlook 
- Outlook (online)

## Install 

```
npm install node-add-to-calendar
```

## Examples

Generate a link for Google Calendar: 
```javascript
const nodeAddToCal = require('node-add-to-calendar');

const address = 'Google, googleville 123 CA'
const eventData = {
  title: 'Meeting',
  startTime: new Date('7-15-19 12:00'),
  description: `Very important meeting at: \n\n${address}`,
  address
}
nodeAddToCal.googleLink(eventData);
// Returns: https://calendar.google.com/calendar/r/eventedit?dates=20190715T120000/20190715T123000&location=Google, googleville 123 CA&text=Meeting&details=Very important meeting at: %0D%0A%0D%0AGoogle, googleville 123 CA
```

Generate an ics file (iCal, Outlook etc.) and download it: 
```javascript
const fs = require('fs');
const path = require('path');
const nodeAddToCal = require('node-add-to-calendar');

nodeAddToCal.getIcs(
  {
    title: 'Job Interview',
    startTime: new Date('3-20-20 9:00'),
    duration: 45,
  },
  value => {
    fs.writeFileSync(path.join(path.dirname(process.mainModule.filename), '..', 'events', 'event.ics'), value);
  },
  [err => console.log(err), err => errorHandler(err)],
  () => {
    res.status(200).download(path.join(path.dirname(process.mainModule.filename), '..', 'events', 'event.ics'));
});

const errorHandler = () => {
  // Do magic here
}
```

## API

node-add-to-calendar provides three different functions: 
*Google Calendar*
```javascript
addToCal.googleLink()
```
*Outlook Online*
```javascript
addToCal.outlookLink()
```
*iCal, Outlook, Windows Calendar*
```javascript
addToCal.getIcs()
```

Google and Outlook both take a javascript object as their only parameter. Keys:

| Key       | data type           | default  |
| ------------- |:-------------:| -----:|
| title      | string      |   '' |
| startTime      | date | now |
| duration | int (in minutes)      |    30 |
| description      | string      |   '' |
| address      | string      |   '' |

The ics file takes the same object, but it also takes success, error and a callback function: 

```javascript
addToCal.getIcs({}, success, error, cb)
```

Success and error can be either a function or an array of functions that should be called when it either succeeds or fails. The value/error is passed to these functions.