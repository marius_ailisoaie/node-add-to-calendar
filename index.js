const moment = require('moment');
const ics = require('ics');

const handleEventData = eventData => {
  const dataObj = {
    startTime: Date.now(),
    address: '',
    duration: 30,
    title: '',
    description: ''
  }
  return Object.assign(dataObj, eventData); 
}

const handleDescriptionFormatting = description => {
  return description.replace(new RegExp(/\n/, 'g'), '%0D%0A');
}

const googleLink = (eventData={}) => {
  const { startTime, address, duration, title, description } = handleEventData(eventData)
  return `https://calendar.google.com/calendar/r/eventedit?dates=${moment(startTime).format('YYYYMMDD[T]HHmm[00]')}/${moment(startTime).add(duration, 'm').format('YYYYMMDD[T]HHmm[00]')}&location=${address}&text=${title}&details=${handleDescriptionFormatting(description)}`
};

const outlookLink = (eventData={}) => {
  const { startTime, address, duration, title, description } = handleEventData(eventData)
  return `https://outlook.live.com/owa/?rru=addevent&startdt=${moment(startTime).format('YYYYMMDD[T]HHmm[00]Z')}&enddt=${moment(startTime).add(duration, 'm').format('YYYYMMDD[T]HHmm[00]Z')}&subject=${title}&location=${address}&body=${handleDescriptionFormatting(description)}`
};

const getIcs = async (eventData, success, err, cb) => {
  const { startTime, address, duration, title, description } = handleEventData(eventData)

  await ics.createEvent({
    title,
    description,
    location: address,
    start: moment(startTime).format('YYYY-M-D-H-m').split("-"),
    startType: 'local',
    duration: { minutes: duration }
    }, (error, value) => {
    if (error) {
      if (Array.isArray(err)) {
        err.forEach(errHandler => errHandler(error))
      } else {
        err(error)
      }
    } else {
      if (Array.isArray(success)) {
        success.forEach(successHandler => successHandler(value))
      } else {
        success(value)
      }
    }
  });
  
  cb();
}

module.exports = {
  googleLink,
  outlookLink,
  getIcs
}